

<?php 
//Kasutaja registreerimine
//Kasutatud kirjandus:
//http://blogforsite.blogspot.com/2012/05/simple-registration-form-using-php.html#.VUyFbpMy5J5


require_once 'functions.php';

session_starter();

    $nameErr="";
    $passErr="";
    $passErr2="";
    $matchErr="";
    $username="";
    $password="";
    $password2="";
    
if ($_SERVER["REQUEST_METHOD"] == "POST"){
    
    $username=trim($_POST['username']);
    $password=trim($_POST['password']);
    $password2=trim($_POST['password2']);
    
    if(empty($username))
    {
        $nameErr = "Please enter a username.";

    }
    if(empty($password)){
        
       $passErr = "Please enter a password.";
       
    }
    if(empty($password2) & !empty($password) & !empty($username)){
        
       $passErr2 = "Please retype the password.";
       
    }
    if(strcmp($password,$password2)!==0 & !empty($password) & !empty($password2) & !empty($username)){
        
        $matchErr="Passwords don't match.";
        
    }
    if(strcmp($password,$password2) ==0 & !empty($password) & !empty($password2) & !empty($username)){
        $username2=mysql_real_escape_string($username);
        $password3=mysql_real_escape_string($password);
        
        $query = "SELECT * FROM users WHERE username='$username2'";
        $result = mysql_query($query);
        
        if (mysql_num_rows($result) >= 1){
            
            echo "<b>Username already exists.</b>";
            
        }
        if(mysql_num_rows($result) == 0){
            $query2 = "INSERT INTO users( username, password ) VALUES ('$username2',SHA1('$password3'))";
            $insert = mysql_query($query2) or die("".mysql_error());
                
            echo "<p><b>Signup successful.</b></p>";
  
        }
        if (!$result) {
                die("Can't add user.");
        }
    }
}
?>

<p><b>Fields marked with * are required.</b></p>

<form action="" method="POST">
<fieldset>
    <label for='username'>Username: *</label>
    <br><input type="text" name="username" maxlength="20" id="username" value="<?php echo htmlspecialchars($username);?>">
    <span class="error"><?php echo $nameErr;?></span>
    <br><label for='password'>Password: *</label>
    <br><input type="password" name="password" maxlength="40" id="password">
    <span class="error"><?php echo $passErr;?></span>
    <br><label for='password2'>Retype password: *</label>
    <br><input type="password" name="password2" maxlength="40" id="password2">
    <span class="error"><?php echo $passErr2;?></span>
    <span class="error"><?php echo $matchErr;?></span>
    <br><input name="submit" type="submit" value=" Register ">
</fieldset>
</form>