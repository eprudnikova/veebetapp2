<?php 

//Sessiooni algatamine

function session_starter(){
    if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
}

//Ühendumine andmebaasiga

function connect_db(){
    global $db_connection;
    $hostname = "localhost";
    $user = "root";
    $password = "";
    $database = "veeb3";
    $db_connection = mysql_connect($hostname, $user, $password) or die("Could not connect to database.");
    mysql_select_db($database, $db_connection) or die("Could not select database.");
}
        
//Funktsioon piltide esitamiseks

$gallery=array("img/1.JPG", "img/2.jpg", "img/3.jpg", "img/4.jpeg", "img/5.jpg", "img/6.jpg", "img/7.jpg", "img/8.jpg");

function display_gallery($gallery){
    foreach ($gallery as $value) {
        echo "<img src=\"$value\" width=\"150px\" height=\"150px\">";       
    }
}

//Funktsioon tabeli esitamiseks lehel index.html
//Abimaterjal: http://webcheatsheet.com/php/multidimensional_arrays.php

$paintings=array(
    array("Painting","Year","Technique"),
    array("Ice Floes, Misty Morning","1894","Painting - oil on canvas"),
    array("View near Rouelles","1858","Painting - oil on canvas"),
    array("Path in the Ile Saint-Martin, Vetheuil","1860","Painting - oil on canvas"),
    array("Corner of a Studio","1861","Painting - oil on canvas"),
    array("Still Life with Bottle, Carafe, Bread and Wine","1862","Painting - oil on canvas"),
    array("Farmyard in Normandy","1863","Painting - oil on canvas"),
    array("Hauling a Boat Ashore, Honfleur","1864","Painting - oil on canvas"),
    array("Candles on the Hearth","1864","Painting - oil on canvas"),
    array("Spring Flowers","1864","Painting - oil on canvas")
);

function paint_table($paintings){
    echo "<table>";
    for($row=0; $row<10; $row++){
        echo "<tr>";
        for($col=0; $col<3; $col++){
            echo "<td>".$paintings[$row][$col]."</td>";
        }
        echo "</tr>";
    }
    echo "</table>";
}


//Väljalogimise funktsioon

function logout(){
    $_SESSION = array();
    if (isset($_COOKIE[session_name()])){ 
        setcookie( session_name(), '', time()-3600, '/' );        
    }
    session_destroy(); 
    header("Location:index.php");
}

//Kontrolli, kas kasutaja on sisse logitud

function check_login(){
if ( $_SESSION['logged_in'] != "y" ) {
    header("Location:index.php");
    return false;
}
return true;
}

?>


