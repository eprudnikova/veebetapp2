<?php
//Maalide tabeli kuvamine
//Kasutatud kirjandus:
//http://www.phpeasystep.com/mysql/9.html
//http://www.killersites.com/community/index.php?/topic/3064-basic-php-system-view-edit-add-delete-records-with-mysqli/
//http://www.phpsuperblog.com/php/html-form-drop-down-menu-with-data-from-mysql-datebase-as-options/
//http://www.yourwebskills.com/mysqldropdown.php


require_once 'functions.php';

session_starter();
?>

<h2>Editable table.</h2>
<h3>Table I. Paintings</h3>

<table>
<?php 

//Admin näeb kogu tabelit, kasutajad näevad ainult enda lisatud ridu

$owner = $_SESSION['username'];

if (($_SESSION['username'] == 'admin') & (isset($_SESSION['username']))){
    $querya = "SELECT eprudnik_paintings.id, eprudnik_paintings.title, eprudnik_paintings.years, eprudnik_paintings.author,  eprudnik_costs.cost 
    FROM eprudnik_costs INNER JOIN eprudnik_paintings ON eprudnik_paintings.id = eprudnik_costs.p_id";
    $result = mysql_query($querya);
}

if(isset($_SESSION['username']) & ($_SESSION['username'] != 'admin')){
    
$query = "SELECT eprudnik_paintings.id, eprudnik_paintings.title, eprudnik_paintings.years, eprudnik_paintings.author,  eprudnik_costs.cost 
    FROM eprudnik_costs INNER JOIN eprudnik_paintings ON eprudnik_paintings.id = eprudnik_costs.p_id 
    INNER JOIN owners ON eprudnik_paintings.id = owners.o_id WHERE owners.owner='$owner'";  
$result = mysql_query($query);

}

if (!$result) {
    die("Can't select from table.");
}
?>
<tr>
<th align="center">Id</th>
<th align="center">Title</th>
<th align="center">Year</th>
<th align="center">Author</th>
<th align="center">Cost</th>
</tr>

<?php
while ($row=mysql_fetch_array($result)){
    echo '<tr><td>'.htmlspecialchars($row['id']).'</td>';
    echo '<td>'.htmlspecialchars($row['title']).'</td>';
    echo '<td>'.htmlspecialchars($row['years']).'</td>';
    echo '<td>'.htmlspecialchars($row['author']).'</td>';
    echo '<td>'.htmlspecialchars($row['cost']).'</td></tr>';
}
?>         
</table>

<?php 

//Teadete kuvamine kasutajale


if(!empty($_SESSION['message']) || !empty($_SESSION['message2'])|| !empty($_SESSION['message3']) || !empty($_SESSION['message4'])){
    
    if(!empty($_SESSION['message'])){
        echo '<p style="color:red">' .$_SESSION['message']. '</p>';
        $_SESSION['message'] = null;
    }
    if(!empty($_SESSION['message2'])){
        echo '<p style="color:red">' .$_SESSION['message2']. '</p>';
        $_SESSION['message2'] = null;
    }
    if(!empty($_SESSION['message3'])){
        echo '<p style="color:red">' .$_SESSION['message3']. '</p>';
        $_SESSION['message3'] = null;
    }
    if(!empty($_SESSION['message4'])){
        echo '<p style="color:red">' .$_SESSION['message4']. '</p>';
        $_SESSION['message4'] = null;
    }
}

$titleErr = $yearErr = $authorErr = $costErr = $title = $year = $author = "";

//Rea lisamine tabelisse

if (!empty($_POST['add-submit'])){
    
    $_POST['title']=trim($_POST['title']);
    $_POST['year']=trim($_POST['year']);
    $_POST['author']=trim($_POST['author']);
    $_POST['cost']=trim($_POST['cost']);
    
     if(empty($_POST['title']))
    {
         $titleErr="Please enter a title.";
    }
    if(empty($_POST['year'])){
        $yearErr = "Please enter a year.";
    }
    if(empty($_POST['author'])){
        $authorErr = "Please enter an author.";
    }
    if(empty($_POST['cost'])){
        $costErr = "Please enter the cost.";
    }
    if(!empty($_POST['title']) & !empty($_POST['year']) & !empty($_POST['author']) & !empty($_POST['cost'])){
        $title=mysql_real_escape_string(htmlentities($_POST['title'],ENT_SUBSTITUTE));
        $year=mysql_real_escape_string(htmlentities($_POST['year'],ENT_SUBSTITUTE));
        $author=mysql_real_escape_string(htmlentities($_POST['author'],ENT_SUBSTITUTE));
        $cost=mysql_real_escape_string(htmlentities($_POST['cost'],ENT_SUBSTITUTE));
        $owner = $_SESSION['username'];
        
        $query = "INSERT INTO eprudnik_paintings( title, years, author ) VALUES ('$title', '$year', '$author')";
        $insert = mysql_query($query) or die("".mysql_error());
        $p_id = mysql_insert_id();
        
        $query2 = "INSERT INTO eprudnik_costs (cost, p_id) VALUES ('$cost','$p_id')";
        $insert2 = mysql_query($query2) or die("".mysql_error());
        
        $query3 = "INSERT INTO owners (owner, o_id) VALUES ('$owner','$p_id')";
        $insert3 = mysql_query($query3) or die("".mysql_error());
        
        $_SESSION['message'] = 'Row added.';
        header('Location: '.$_SERVER['REQUEST_URI']);

    }
}

//Tabeli muutmine, ainult üks väli peab olema täidetud

$title2 = $year2 = $author2 = $idErr = $id = "";

if (!empty($_POST['edit-submit'])){
      
    $_POST['selectid']=trim($_POST['selectid']);
    $_POST['title2']=trim($_POST['title2']);
    $_POST['year2']=trim($_POST['year2']);
    $_POST['author2']=trim($_POST['author2']);
    $_POST['cost2']=trim($_POST['cost2']);
    
    if(empty($_POST['selectid']))
    {
         $idErr="Please enter an id.";
    }
    if(!empty($_POST['selectid']) & empty($_POST['year2']) & empty($_POST['author2']) & empty($_POST['title2']) & empty($_POST['cost2'])){
        
        $idErr="Please enter at least one value.";
    }
    if(!empty($_POST['selectid']) & !empty($_POST['year2']) || !empty($_POST['author2']) || !empty($_POST['title2']) || !empty($_POST['cost2'])){
        
        $id=mysql_real_escape_string(htmlentities($_POST['selectid'],ENT_SUBSTITUTE));
        $title2=mysql_real_escape_string(htmlentities($_POST['title2'],ENT_SUBSTITUTE));
        $year2=mysql_real_escape_string(htmlentities($_POST['year2'],ENT_SUBSTITUTE));
        $author2=mysql_real_escape_string(htmlentities($_POST['author2'],ENT_SUBSTITUTE));
        $cost2=mysql_real_escape_string(htmlentities($_POST['cost2'],ENT_SUBSTITUTE));
        
        if(!empty($_POST['title2'])){
            
            $query = "UPDATE eprudnik_paintings SET title = '$title2' WHERE id = '$id'";
            $insert = mysql_query($query) or die("".mysql_error());
            $_SESSION['message2'] = 'Table edited.';
            header('Location: '.$_SERVER['REQUEST_URI']);
        }
        if(!empty($_POST['year2'])){
            
            $query = "UPDATE eprudnik_paintings SET years = '$year2' WHERE id = '$id'";
            $insert = mysql_query($query) or die("".mysql_error());
            $_SESSION['message2'] = 'Table edited.';
            header('Location: '.$_SERVER['REQUEST_URI']);
        }
        if(!empty($_POST['author2'])){
            
            $query = "UPDATE eprudnik_paintings SET author = '$author2' WHERE id = '$id'";
            $insert = mysql_query($query) or die("".mysql_error());
            $_SESSION['message2'] = 'Table edited.';
            header('Location: '.$_SERVER['REQUEST_URI']);
        }
        if(!empty($_POST['cost2'])){
            
            $query = "UPDATE eprudnik_costs SET cost = '$cost2' WHERE p_id = '$id'";
            $insert = mysql_query($query) or die("".mysql_error());
            $_SESSION['message2'] = 'Table edited.';
            header('Location: '.$_SERVER['REQUEST_URI']);
        }
    }
}

//Kustuta rida tabelist, peab olema admin

if (!empty($_POST['delete-submit'])){
      
    if($_SESSION['roles'] == 'admin'){
        $pid = mysql_real_escape_string(htmlentities($_POST['selectid'],ENT_SUBSTITUTE));
        
        $query = "DELETE eprudnik_paintings, eprudnik_costs, owners FROM eprudnik_paintings INNER JOIN eprudnik_costs ON
        eprudnik_paintings.id = eprudnik_costs.p_id INNER JOIN owners ON eprudnik_paintings.id = owners.o_id WHERE eprudnik_paintings.id = '$pid'";
        
        $delete = mysql_query($query) or die("".mysql_error());
        
        $_SESSION['message4'] = 'Row deleted.';
        header('Location: '.$_SERVER['REQUEST_URI']);
    }
    if($_SESSION['roles'] != 'admin'){
        $_SESSION['message3'] = 'You must be logged in as admin to delete a row.';
        header('Location: '.$_SERVER['REQUEST_URI']);
    }
    
}

?>

<p>Add new row. All fields are required.</p>

<form name="add" action="" method="POST"> 
    <label for='title'>Title:</label>
    <br><input type="text" name="title" maxlength="100" id="title" value="<?php echo htmlspecialchars($title);?>">
    <span class="error"><?php echo htmlspecialchars($titleErr);?></span>
    <br><label for='year'>Year:</label>
    <br><input type="number" name="year" min="1" max="2015" id="year" value="<?php echo htmlspecialchars($year);?>">
    <span class="error"><?php echo htmlspecialchars($yearErr);?></span>
    <br><label for='author'>Author:</label>
    <br><input type="text" name="author" maxlength="60" id="author" value="<?php echo htmlspecialchars($author);?>">
    <span class="error"><?php echo htmlspecialchars($authorErr);?></span>
    <br><label for='cost'>Cost:</label>
    <br><input type="number" name="cost" min="0" id="acost" value="<?php echo htmlspecialchars($cost);?>">
    <span class="error"><?php echo htmlspecialchars($costErr);?></span>
    <br><input name="add-submit" type="submit" value=" Add ">
</form>

<p>To edit the table, please choose row id number and at least one field.</p>

<form name="edit" action="" method="POST"> 
    <label for='id'>Id:</label><br>
    <select id= "selectid" name="selectid">
    <?php 
    
    $owner2 = $_SESSION['username'];
    
    if ($_SESSION['username'] == 'admin'){
        
        $queryb ="SELECT id FROM eprudnik_paintings";
        $result2 = mysql_query($queryb) or die("".mysql_error());      
    }
    if ($_SESSION['username'] != 'admin'){
        
        $query2= "SELECT eprudnik_paintings.id FROM eprudnik_paintings INNER JOIN owners ON eprudnik_paintings.id = owners.o_id
        WHERE owners.owner='$owner2'";    
        $result2 = mysql_query($query2) or die("".mysql_error()); 
    }
      
    while ($row = mysql_fetch_array($result2)) {
            $id = $row['id'];
                echo '<option>'.htmlspecialchars($id).'</option>';
            }
    ?>   
        
    </select>
    <span class="error"><?php echo htmlspecialchars($idErr);?></span>
    <br><label for='title2'>Title:</label>
    <br><input type="text" name="title2" maxlength="100" id="title2" value="<?php echo htmlspecialchars($title2);?>">
    <br><label for='year2'>Year:</label>
    <br><input type="number" name="year2" min="1" max="2015" id="year2" value="<?php echo htmlspecialchars($year2);?>">
    <br><label for='author2'>Author:</label>
    <br><input type="text" name="author2" maxlength="60" id="author2" value="<?php echo htmlspecialchars($author2);?>">
    <br><label for='cost2'>Cost:</label>
    <br><input type="number" name="cost2" min="0" id="acost" value="<?php echo htmlspecialchars($cost2);?>">
    <br><input name="edit-submit" type="submit" value=" Edit ">
</form>

<p>Delete a row. You must be logged in as administrator.</p>

<form name="delete" action="" method="POST"> 
    <label for='id'>Id:</label><br>
    <select id= "selectid" name="selectid">
    <?php 
    
    $owner3 = $_SESSION['username'];
    
    if ($_SESSION['username'] == 'admin'){
        
        $queryc ="SELECT id FROM eprudnik_paintings";
        $result2 = mysql_query($queryc) or die("".mysql_error());       
    }
    if ($_SESSION['username'] != 'admin'){
        
        $query3= "SELECT eprudnik_paintings.id FROM eprudnik_paintings INNER JOIN owners ON eprudnik_paintings.id = owners.o_id
                 WHERE owners.owner='$owner3'";
        $result2 = mysql_query($query3) or die("".mysql_error()); 
    }
    while ($row = mysql_fetch_array($result2)) {
            $id = $row['id'];
                echo '<option>'.htmlspecialchars($id).'</option>';
            }
    ?>   
    </select>
    <input name="delete-submit" type="submit" value=" Delete row "> 
</form>


