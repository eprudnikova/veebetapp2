#Database_veeb3_user_root@localhost_password_empty. Yhendumine baasiga failis functions.php
#Kasutajate tabel

CREATE TABLE IF NOT EXISTS users (
id INT NOT NULL auto_increment,
username varchar(20) NOT NULL,
password char(40) NOT NULL,
PRIMARY KEY(id),
unique(username)
);

ALTER TABLE users ADD COLUMN roles VARCHAR(20) DEFAULT 'user';

INSERT INTO users( id, username, password ) VALUES ('1','user',SHA1('pass'));
INSERT INTO users( id, username, password ) VALUES ('2','bob',SHA1('bob'));
INSERT INTO users( username, password, roles ) VALUES ('admin',SHA1('admin'),'admin');

#Maalide tabel

CREATE TABLE IF NOT EXISTS eprudnik_paintings (
id INT NOT NULL auto_increment,
title varchar(100) NOT NULL,
years INT NOT NULL,
author varchar(60) NOT NULL,
PRIMARY KEY(id)
);

#Hindade tabel

CREATE TABLE IF NOT EXISTS eprudnik_costs (
id INT NOT NULL auto_increment,
cost INT NOT NULL,
p_id INT,
FOREIGN KEY (p_id) REFERENCES eprudnik_paintings(id) ON DELETE CASCADE,
PRIMARY KEY(id)
);

#SELECT * FROM users;
#SELECT * FROM eprudnik_paintings;
#SELECT * FROM eprudnik_costs;

INSERT INTO eprudnik_paintings(title, years, author) VALUES ('Ice Floes, Misty Morning','1894','Monet');
INSERT INTO eprudnik_paintings(title, years, author) VALUES ('Corner of a Studio','1865','Monet');
INSERT INTO eprudnik_paintings(title, years, author) VALUES ('View near Rouelles','1832','Monet');
INSERT INTO eprudnik_paintings(title, years, author) VALUES ('Still Life with Bottle','1861','Monet');
INSERT INTO eprudnik_paintings(title, years, author) VALUES ('Path in the Ile Saint-Martin','1874','Monet');

INSERT INTO eprudnik_costs(cost, p_id) VALUES ('10000','1');
INSERT INTO eprudnik_costs(cost, p_id) VALUES ('20000','2');
INSERT INTO eprudnik_costs(cost, p_id) VALUES ('30000','3');
INSERT INTO eprudnik_costs(cost, p_id) VALUES ('40000','4');
INSERT INTO eprudnik_costs(cost, p_id) VALUES ('50000','5');

#SELECT eprudnik_paintings.id, eprudnik_paintings.title, eprudnik_paintings.years, eprudnik_paintings.author,  eprudnik_costs.cost FROM eprudnik_costs INNER JOIN eprudnik_paintings ON eprudnik_paintings.id = eprudnik_costs.p_id;

#DELETE eprudnik_paintings, eprudnik_costs FROM eprudnik_paintings INNER JOIN eprudnik_costs ON
#eprudnik_paintings.id = eprudnik_costs.p_id WHERE eprudnik_paintings.id ='2';

#SELECT * FROM eprudnik_paintings INNER JOIN eprudnik_costs ON
#eprudnik_paintings.id = eprudnik_costs.p_id WHERE eprudnik_costs.p_id ='17';

#SELECT SUM(cost) AS Total FROM eprudnik_costs;

#Maalide veeru omanike tabel

CREATE TABLE IF NOT EXISTS owners (
id INT NOT NULL auto_increment,
owner varchar(20) NOT NULL,
o_id INT,
FOREIGN KEY (o_id) REFERENCES eprudnik_paintings(id) ON DELETE CASCADE,
PRIMARY KEY(id)
);

INSERT INTO owners (o_id, owner) VALUES ('1','admin');
INSERT INTO owners (o_id, owner) VALUES ('2','admin');
INSERT INTO owners (o_id, owner) VALUES ('3','admin');
INSERT INTO owners (o_id, owner) VALUES ('4','user');
INSERT INTO owners (o_id, owner) VALUES ('5','user');

#SELECT * FROM owners;